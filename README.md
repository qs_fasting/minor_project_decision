# Minor Project Decision #

Minor Project Decision is a web application created for assigning projects to multiple teams based on the ordinal 
preference of the  available projects.  


### Python Dependencies ###

Minor Project Decision is written in **Python 3.6.2** and requires at least **Python 3.4** to run.

* Flask

```
pip3 install Flask
```

* sqlite3
* wtforms
* passlib

### Adding Projects ###

Projects can be added when logged in, a form with a title entry and body text area, these need to be filled out. Projects are user
specific.


### Adding Users ####

use the included admins.py

````buildoutcfg
admins.py -u --user [USERNAME] -n --name [displayname] -r [flag remove user]
````

adding a user
````buildoutcfg
admins.py -u --user [USERNAME] -n --name [displayname]

you will be prompted to enter a password.
````

removing a user
````buildoutcfg
admins.py -u --user [USERNAME] -r [flag remove user]

before being able to remove a user, you will be prompted to the users password.
````


### Running Minor Project Decision ###

just run minorprod.py and the application will start.
it is possible to run with appache but I've yet to look in to that.

### Bugs ###

When a team submits a invalid form, the team is still added to the database with their choices.
* if issue persists send a mail, though the bug can't be reproduced by dev.

### Version ###

Version: `1.0`

Codename: `Voa`

### Who do I talk to? ###

This repo was developed by:

* R.H.J. Vedder (rhjvedder)

For the **Hanze University of Applied Science Groningen** (Hanze) as part of voluntary work for the **Bioinformatics**
department.

**Copyright (C) 2017**  R.H.J. Vedder

![Hanze Logo](https://www.hollandisc.com/-/media/ISC/Holland%20ISC/Images/Hanze%20logo%20160%20x%20400.jpg)