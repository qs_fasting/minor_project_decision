#!/usr/bin/env python3

"""
    Copyright (C) 2017  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import random
import sys


class DevideProjects:

    def __init__(self, teams, projects):
        self.teams = teams
        self.projects = projects

    def devide(self):
        n_proj_dev = 0
        for n in range(len(self.projects)):
            if n_proj_dev == len(self.teams):
                break
            else:
                chosen = {}
                for team in self.teams:
                    if team.project == "":
                        if team.choices[n] in chosen:
                            chosen[team.choices[n]].append(team)
                        else:
                            chosen[team.choices[n]] = [team]
                for k, v in chosen.items():
                    if len(v) > 1:
                        lucky = random.choice(v)
                    else:
                        lucky = v[0]
                    for project in self.projects:
                        if project.id_ == int(k):
                            lucky.project = project.title
                            n_proj_dev += 1
