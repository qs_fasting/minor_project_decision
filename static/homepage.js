/*
 * Autor: Rolf Vedder rolf.vedder@gmail.com
 */

$(document).ready(function()
{
    var members = 1;

    $(document).on('click', '#add', function() {
        if (members < 5) {
            members += 1;
            // add a member form field
            $('#team_members').append('<div id="member_name' + members + '" class="pt-2">'
            + '<span class="ml-3"><label for="teammembers-0-member">Member ' + members + '</label></span>'
            + '<input class="form-control" id="teammembers-' + (members - 1) + '-member" label="None" name="teammembers-' + (members - 1) + '-member" type="text" value="">'
            + '</div>');
        }
    });

    $(document).on('click', '#remove', function() {
        if (members > 1) {
            // remove a member form field
            $('#member_name' + members).remove();
            members -= 1;
        }
    });

    $('.delete-team').click(function() {
        var team = this.id
        var data = {'team-id': team}
        $(this).closest('table').remove()
        $.ajax({
          type : 'POST',
          url : "/delete-team",
          contentType: 'application/json;charset=UTF-8',
          data : JSON.stringify(data, null, '\t')
        });
        return false;
    });

    $('.delete').click(function() {
        var project = this.id
        var data = {'project_title': project}
        var next = $(this).parent()
        $(next).parent().remove()
        $.ajax({
          type : 'POST',
          url : "/delete-project",
          contentType: 'application/json;charset=UTF-8',
          data : JSON.stringify(data, null, '\t')
        });
        return false;
    });

    $('#divider_of_projects').click(function() {
        $.ajax({
          type : 'POST',
          url : "/process",
          contentType: 'application/json;charset=UTF-8',
          data : JSON.stringify({'devide_projects': 1}, null, '\t'),
          complete: function(data) {
            data = JSON.parse(data.responseText);
            $('.status').html("");
            $('.status').addClass("alert alert-info");
            $('.status').append(data.status)
          }
        });
        return false;
    });

    $('#teachers').on('change', function(event) {
        var val = $(this).val();
        $.ajax({
          type : 'POST',
          url : "/projects",
          contentType: 'application/json;charset=UTF-8',
          data : JSON.stringify({'get_projects': val}, null, '\t'),
          complete: function(data) {
            data = JSON.parse(data.responseText);
            $('.projects').html("");
            $('.projects').append(data.projects);
            $('.choices').html("");
            $('.choices').append(data.choices);
          }
        });
        return false;
    });
});
