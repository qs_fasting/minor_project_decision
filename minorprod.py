#!/usr/bin/env python3

"""
    Copyright (C) 2017  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Flask, render_template, flash, redirect, url_for, session, request, g
from functools import wraps
from wtforms import Form, StringField, PasswordField, validators, FormField, FieldList, SelectField, TextAreaField
import sqlite3
import re
from passlib.hash import sha256_crypt
import json
from utils import Devider
import sys


# Config and globals
DATABASE = './database/MPD.db'
app = Flask(__name__)
projects = []
teams = []
project_choices = []
project_zero = (0, "Select each project once...")
teachers = []
teacher_choices = []
teacher_zero = (0, "Select your teacher...")


# Data Classes
class Team:

    def __init__(self, id_, members, choices, teacher, project="", n=0):
        self.n = n
        self.id_ = id_
        self.members = members
        self.choices = choices
        self.project = project
        self.teacher = teacher


class Project:

    def __init__(self, id_, title, body, teacher):
        self.title = title
        self.body = body
        self.teacher = teacher
        self.id_ = int(id_)


class Teacher:

    def __init__(self, id_, name):
        self.id_ = id_
        self.name = name


# Form classes
class SigninForm(Form):
    username = StringField(id='Username', validators=[validators.Length(min=4, max=25), validators.DataRequired()],
                           render_kw={"placeholder": "Username"}, label=None)
    password = PasswordField(id='Password', validators=[validators.Length(min=8, max=25), validators.DataRequired()],
                             render_kw={"placeholder": "Password"}, label=None)


class MemberForm(Form):
    member = StringField('Member 1', validators=[validators.Length(min=1, max=30)])


class ProjectsForm(Form):
    project = SelectField(coerce=int, choices=project_choices, validators=[validators.NoneOf([0],
                                                                                             message="Make a choice")])


class ChoicesForm(Form):
    teachers = SelectField(coerce=int, choices=teacher_choices, validators=[validators.NoneOf([0],
                                                                                             message="Make a choice")])
    teammembers = FieldList(FormField(MemberForm), min_entries=1, max_entries=5)
    c_projects = FieldList(FormField(ProjectsForm))


class AddProjectForm(Form):
    title = StringField('Title', validators=[validators.Length(min=1, max=50)])
    body = TextAreaField('Body', validators=[validators.Length(min=50)])


# Helper Functions
def home_error(form, **kwargs):
    for n, sub_form in enumerate(form.c_projects):
        sub_form.project.label = "Choice " + str(n + 1)
    return render_template("home.html", form=form, projects=projects, **kwargs)


# database functions
def get_projects(teacher_id):
    # clearing the project lists
    projects.clear()
    project_choices.clear()
    db = get_db()
    curr = db.cursor()
    if sec_int(teacher_id):
        curr.execute("select rowid, title, body, teacher_id from Projects where teacher_id=?", (teacher_id,))
        [projects.append(Project(r[0], r[1], r[2], r[3])) for r in curr.fetchall()]
        # adding in the placeholder
        project_choices.append(project_zero)
        [project_choices.append((p.id_, p.title)) for p in projects]


def get_teams(teacher_id):
    # clearing the team list
    teams.clear()
    db = get_db()
    curr = db.cursor()
    if sec_int(teacher_id):
        curr.execute("select rowid, names, choices, teacher_id from Teams where teacher_id=?", (teacher_id,))
        [teams.append(Team(r[0], r[1].split(", "), r[2].split(", "), r[3])) for r in curr.fetchall()]
        n = 0
        for team in teams:
            n += 1
            team.n = n


def get_teachers():
    # clear the teachers list
    teachers.clear()
    teacher_choices.clear()
    teacher_choices.append(teacher_zero)
    db = get_db()
    curr = db.cursor()
    curr.execute("select rowid, name from Admins")
    [teachers.append(Teacher(r[0], r[1])) for r in curr.fetchall()]
    [teacher_choices.append((t.id_, t.name)) for t in teachers]


def get_teacher_id(username):
    db = get_db()
    curr = db.cursor()
    curr.execute("select rowid from Admins where user=?", (username,))
    return curr.fetchone()[0]


def remove_project(project_id):
    try:
        project_id = int(project_id)
        if sec_int(project_id):
            db = get_db()
            curr = db.cursor()
            curr.execute("delete from Projects where rowid=?", (project_id,))
            db.commit()
    except:
        flash("Wrong value!", "danger")


# database sql injection prevention
def sec_string(string):
    return re.search("[^a-zA-Z0-9_ ]", string)


def sec_int(integer):
    return isinstance(integer, int)


# wrapper to prevent page load without being logged in
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please try logging in.', 'danger')
            return redirect(url_for('login'))
    return wrap


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


# Page routes
@app.route('/', methods=['GET', 'POST'])
def choice_selection():
    get_teachers()
    form = ChoicesForm(request.form)
    if request.method == 'POST' and form.validate():
        chosen = []
        members = []
        teacher_id = form.teachers.data
        if not isinstance(teacher_id, int):
            return home_error(form=form, error="Invalid form value!")
        for choice in form.c_projects.data:
            for k, v in choice.items():
                if isinstance(v, int):
                    chosen.append(v)
                else:
                    return home_error(form=form, error="Invalid form value!")
        for choice in chosen:
            if chosen.count(choice) > 1:
                return home_error(form=form, error="Choose each project only once!")
        for member in form.teammembers.data:
            for k, v in member.items():
                if sec_string(v):
                        return home_error(form=form, error="Names can only exist out of letters,"
                                                           " numbers, underscores and dashes.")
                members.append(v)
        db = get_db()
        curr = db.cursor()
        curr.execute("SELECT names, choices FROM Teams where names=? and teacher_id=?",
                     (", ".join(members), teacher_id))
        if curr.fetchone():
            return home_error(form=form, error="That team for this teacher has already been submitted!")
        curr.execute("INSERT INTO Teams (names, choices, teacher_id) VALUES (?,?,?)",
                     (", ".join(members), ", ".join([str(choice) for choice in chosen]), teacher_id))
        db.commit()
        db.close()
        return redirect("division")
    elif request.method == 'POST' and not form.validate():
        return home_error(form=form, error=None)
    else:
        form = ChoicesForm(c_projects=[x + 1 for x, v in enumerate(projects)])
        return home_error(form=form, error=None)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = SigninForm(request.form)
    if request.method == 'POST' and form.validate():
        cur = get_db().cursor()
        try:
            username = form.username.data
            if sec_string(username):
                flash("Usernames can only exist out of letters, numbers, underscores and dashes.", "danger")
                return render_template("login.html", form=form)
            cur.execute("SELECT pass FROM Admins WHERE user=?", (username,))
            hashed = cur.fetchone()[0]
            cur.close()
            if sha256_crypt.verify(form.password.data, hashed):
                # show a dashboard
                session['logged_in'] = True
                session['username'] = username
                flash("You are now logged in", 'success')
                return redirect(url_for('dashboard'))
                pass
            else:
                # show an error message wrong password
                error = "Username and/or password is incorrect."
                return render_template("login.html", error=error, form=form)
        except:
            # show an error message user not known
            error = "Username and/or password is incorrect."
            return render_template("login.html", error=error, form=form)
    return render_template("login.html", form=form)


@app.route('/dashboard')
@is_logged_in
def dashboard():
    get_teams(get_teacher_id(session["username"]))
    get_projects(get_teacher_id(session["username"]))
    return render_template('dashboard.html', projects=projects, teams=teams)


@app.route('/add_project', methods=['GET', 'POST'])
@is_logged_in
def add_project():
    form = AddProjectForm(request.form)
    if request.method == "POST" and form.validate():
        db = get_db()
        curr = db.cursor()
        title = form.title.data
        body = form.body.data
        body = body.replace("<p>", "")
        body = body.replace("</p>", "")
        teacher_id = get_teacher_id(session["username"])
        curr.execute("select rowid from Projects where title=? and teacher_id=?", (title, teacher_id))
        if curr.fetchone():
            return render_template('add_project.html', form=form, error="This project already exists!")
        else:
            curr.execute("INSERT INTO Projects (title, body, teacher_id) VALUES (?,?,?)", (title, body, teacher_id))
            db.commit()
            return redirect(url_for("dashboard"))
    return render_template('add_project.html', form=form)


@app.route('/delete-project', methods=['POST'])
@is_logged_in
def delete_a_project():
    project_id = request.json["project_title"].replace("delete_", "")
    remove_project(project_id)
    get_projects(get_teacher_id(session["username"]))
    return json.dumps({'status': 'OK'})


@app.route('/delete-team', methods=['POST'])
@is_logged_in
def delete_a_team():
    team_id = request.json["team-id"].replace("delete_", "")
    try:
        team_id = int(team_id)
        db = get_db()
        db.execute("DELETE FROM Teams where rowid=?", (team_id,))
        db.commit()
        get_teams(get_teacher_id(session["username"]))
        return json.dumps({'status': 'OK'})
    except:
        return json.dumps({'status': 'not OK'})


@app.route('/process', methods=['POST'])
@is_logged_in
def process():
    teacher_id = get_teacher_id(session["username"])
    get_teams(teacher_id)
    get_projects(teacher_id)
    if request.json["devide_projects"]:
        Devider.DevideProjects(teams, projects).devide()
        return json.dumps({'status': 'Finished dividing the projects.'})
    else:
        return json.dumps({'status': 'Your Algorithm is shit!'})


@app.route('/projects', methods=['POST'])
def get_project():
    teacher_id = int(request.json["get_projects"])
    if teacher_id == 0:
        return json.dumps({'projects': 'Select a teacher to view available projects.'})
    else:
        get_projects(teacher_id)
        form = ChoicesForm(c_projects=[x + 1 for x, v in enumerate(projects)])
        for n, sub_form in enumerate(form.c_projects):
            sub_form.project.label = "Choice " + str(n + 1)
        return json.dumps({'projects': render_template("includes/_projects_home.html", projects=projects),
                           'choices': render_template("includes/_choices_home.html", form=form)})


@app.route('/division')
def division():
    return render_template('division.html', teams=teams)


@app.route('/logout')
def logout():
    session.clear()
    return redirect("/")


if __name__ == "__main__":
    app.secret_key = 'buggs_bunny'
    app.run(debug=True)