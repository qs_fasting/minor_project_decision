#!/usr/bin/env python3

"""
    Copyright (C) 2017  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sqlite3
import argparse
import getpass
from passlib.hash import sha256_crypt

DATABASE = "./database/MPD.db"


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user", required=True, help="The username under which to register the admin.")
    parser.add_argument("-n", "--name", required=False, help="The name to allow students to pick as teacher, for "
                                                             "security purposes shouldn't be the same as the username")
    parser.add_argument("-r", "--remove", action="store_true", help="Flag for removing the admin from the database.")
    return parser.parse_args()


def add_user(username, name, password):
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()
    # Add User
    try:
        cursor.execute("INSERT INTO Admins (user, name, pass) VALUES (?,?,?)", (username, name, password))
    except:
        print("Can't add the same user twice")
    conn.commit()
    conn.close()


def remove_user(username):
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()
    try:
        cursor.execute("SELECT pass FROM Admins WHERE user=?", (username,))
        hashed = cursor.fetchone()[0]
        if sha256_crypt.verify(getpass.getpass("Enter Password: "), hashed):
            cursor.execute("DELETE FROM Admins WHERE user=? and pass=?", (username, hashed))
        else:
            print("Wrong password, try again.")
    except:
        print("The Username specified is not known.")
    conn.commit()
    conn.close()


def main():
    parser = parse()
    username = parser.user
    name = parser.name
    if parser.remove:
        remove_user(username)
    else:
        if name:
            password = sha256_crypt.encrypt(getpass.getpass("Enter Password: "))
            add_user(username, name, password)
            print("Added {}".format(username))
        else:
            print("When adding a user, the screen name has to be included!")


if __name__ == "__main__":
    main()
